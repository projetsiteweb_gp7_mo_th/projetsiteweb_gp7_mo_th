CREATE DATABASE  IF NOT EXISTS `telethonDB`;
USE `telethonDB`;

CREATE TABLE `compte` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `surname` varchar(45) NOT NULL,
  `givenName` varchar(45) NOT NULL,
  `adress` varchar(45) NOT NULL,
  `telefonNumber` varchar(45) NOT NULL,
  `userName` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `activite` varchar(45),
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `appellation` varchar(45) NOT NULL,
  `genre` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

CREATE TABLE `equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `activity` varchar(45) NOT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;




