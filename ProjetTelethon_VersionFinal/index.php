<!DOCTYPE html>
<html>

	<head>
		<title>Esigelec-Telethon- Accueil</title>
		<meta content="text/html;charset=utf-8" http-equiv="content-type">
		<link rel = "stylesheet" type = "text/css" href="css/allstyles.css">
		
	</head>
	
	<body>

	<div id="workspace" class="workspace">

			<div id="HomeBody">

				<table 
					style="table-layout: fixed; margin: 0px auto;">
					<tbody>
						<tr>

							<td class="HomeLeftTDContainer">

								<div style="width: 320px;">

									<div>

										<div id="topcontainer">
											<a class="fimlogo" href="index.php"
												title="Home"></a>
										</div>
									</div>
									
									<img src="images/oser_vaincre.png" alt="Telethon"/>


								</div>
							</td>

							<td >
								<div>
									<div>

										<div>
											
											<div class="menu">
												<ul class="menulinkUL">
													<li class="li_menulink"><a class="menulink_first" href="index.php"
														title="Acceuil">Accueil</a></li>
													<li class="li_menulink"><a class="menulink" href="activites.php"
														title="Activités">Activités</a></li>
													<li class="li_menulink"><a class="menulink" href="inscription.php"
														title="Inscription">Inscription</a></li>
													<li class="li_menulink"><a class="menulink" href="inscrits.php"
														title="Inscrits">Inscrits</a></li>						
													<li class="li_menulink"><a class="menulink" href="connexion.php" 
														title="Connexion">Connexion</a></li>
												</ul>

											</div>
											
											<div class="ClearBoth">
													<span></span>
											</div>
											
											<div class="UIHomePage">

												<div class="TLContainer">

														<div class="overview">
															<div>
																<span class="ctitle" >
																	Esigelec - Telethon 2013
																</span>
																 
															</div>
															
															<br>
															
															<div >
																<span> 
																Bienvenue Sur la page Principale de Esigelec - Telethon 2013.
																<br><br>
                                								Ici vous avez la possibilité de vous s'inscrire dans des activités divers, ou de creer de nouvelles 
                                								activités pour les membres de Telethon 2013.
                                
                                								<br> <br>
                                								
								                                Êtes-vous intéressé? 
								                                
								                                <a title="S'inscrire" href="inscription.php" class="boldtext" >S'inscrire</a>
								                                <br> <br>
																</span> 

															</div>
														</div>
												</div>
												<div class="ClearBoth">
													<span></span>
												</div>
											</div>

										</div>

									</div>

								</div>

							</td>

						</tr>
					</tbody>
				</table>


			</div>

			<div>
				<div >
					<div>
						<div>
							<div>
								<div class="footer">
									<div id="areacontainer">
										<div id="area">

											<a title="Telethon 2013"
												href="index.php">Esigelec - Telethon 2013</a>
											
											- <a title="Contact"
												href="contact.php">Contact</a>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
   </div>
  
  </body>
	
	
</html>