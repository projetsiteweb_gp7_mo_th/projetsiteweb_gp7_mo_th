<?php 
	 session_start();

     $hostname = $_SERVER['HTTP_HOST'];
     $path = dirname($_SERVER['PHP_SELF']);
	 $loggedin= $_SESSION['loggedIn'];
	 $nom= $_SESSION["nom"];
	 $prenom= $_SESSION["prenom"];
	 $adresse= $_SESSION["adresse"];
	 $telephone= $_SESSION["telephone"];
	 $activite= $_SESSION["activite"];
	 
	 $username= $_SESSION['username'];
     if (!isset($loggedin) || ! $loggedin) {
      header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/connexion.php');
      exit;
     }
?> 
<!DOCTYPE html>
<html>

	<head>
		<title>Esigelec-Telethon- Accueil</title>
		<meta content="text/html;charset=utf-8" http-equiv="content-type">
		<link rel = "stylesheet" type = "text/css" href="css/allstyles.css">
		
	</head>
	
	<body>

	<div id="workspace" class="workspace">

			<div id="HomeBody">

				<table 
					style="table-layout: fixed; margin: 0px auto;">
					<tbody>
						<tr>

							<td class="HomeLeftTDContainer">

								<div style="width: 320px;">

									<div>

										<div id="topcontainer">
											<a class="fimlogo" href="index.php"
												title="Home"></a>
										</div>
									</div>
									
									<img src="images/oser_vaincre.png" alt="Telethon"/>


								</div>
							</td>

							<td >
								<div>
									<div>
										<span class="boldtext" style="float:right;padding-right:15px;"> 
												
												<?php 
												if (isset($loggedin) || $loggedin) echo $nom; 
												?> 
										</span>
										<div>
											
											<div class="menu">
												<ul class="menulinkUL">
													<li class="li_menulink"><a class="menulink_first" href="moncompte.php"
														title="Mon compte">Mon compte</a></li>
													<li class="li_menulink"><a class="menulink" href="monequipe.php"
														title="Mon équipe">Mon équipe</a></li>
													<li class="li_menulink"><a class="menulink" href="myactivity.php"
														title="Créer Activité">Créer Activité</a></li>	
													<li class="li_menulink"><a class="menulink" href="reactivity.php"
														title="Rejoindre Activité">Rejoindre Activité</a></li>				
													<li class="li_menulink"><a class="menulink" href="deconnexion.php"
														title="Déconnexion">Déconnexion</a></li>													
												</ul>

											</div>
											
											<div class="ClearBoth">
													<span></span>
											</div>
											
											<div class="UIHomePage">

												<div class="TLContainer">

														<div class="overview">
															<div>
																<span class="ctitle" >
																	Bienvenu dans votre page personel sur Telethon!
																</span>
																 
															</div>
															
															<br>
															
															<div >
																<span class="boldtext" style="color:black;">Mes infos générales:</span><br><br>
																
																<label for="nom" style="width: 105px;float: left;">Nom:</label>
																<input type="text" name="nameIn" id="nom" value="<?php echo $nom; ?>" disabled="disabled"/>
																<br /><br />
																<label for="prenom" style="width: 105px;float: left;">Prénom:</label>
																<input type="text" name="prenomIn" id="prenom" value="<?php echo $prenom; ?>" disabled="disabled"/>
																<br /><br />
																<label for="adress" style="width: 105px;float: left;">Adresse:</label>
																<input type="text" name="AdressIn" id="adress" style="width:20em" value="<?php echo $adresse; ?>" disabled="disabled"/>
																<br /><br />
																<label for="telefon" style="width: 105px;float: left;">Téléphone:</label>
																<input type="text" name="telefonIn" id="telefon" value="<?php echo $telephone; ?>" disabled="disabled"/>
																<br /><br />
																<label for="activity" style="width: 105px;float: left;">Activité:</label>
																<input type="text" name="activityIn" id="activity" value="<?php echo $activite; ?>" disabled="disabled"/>
																<br /><br />
																
																<label for="identifiant" style="width: 105px;float: left;">Identifiant:</label>
																<input type="text" name="identifiantIn" id="identifiant" value="<?php echo $username; ?>" disabled="disabled" />
																<br /><br />

															</div>
														</div>
												</div>
												<div class="ClearBoth">
													<span></span>
												</div>
											</div>

										</div>

									</div>

								</div>

							</td>

						</tr>
					</tbody>
				</table>


			</div>

			<div>
				<div >
					<div>
						<div>
							<div>
								<div class="footer">
									<div id="areacontainer">
										<div id="area">

											<a title="Telethon 2013"
												href="moncompte.php">Esigelec - Telethon 2013</a>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
   </div>
  
  </body>
	
	
</html>