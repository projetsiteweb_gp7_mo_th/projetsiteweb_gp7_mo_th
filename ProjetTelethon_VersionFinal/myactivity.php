﻿<?php 
	 session_start();

     $hostname = $_SERVER['HTTP_HOST'];
     $path = dirname($_SERVER['PHP_SELF']);
	 $loggedin= $_SESSION['loggedIn'];
	 $activite= $_SESSION["activite"];
	 $nom= $_SESSION["nom"];
	 $identifiant= $_SESSION['username'];
     if (!isset($loggedin) || ! $loggedin) {
      header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/connexion.php');
      exit;
     }
     
     if (isset($_POST['newactivity']))
     {
     	$error="";
     	if ($_POST['nameIn']=="")
     	{
     		$error='<p style="color:red">Le champ "Nom" est obligatoire!</p>';
     	}
     	if ($_POST['genreIn']=="")
     	{
     		$error .='<p style="color:red">Le champ "Genre" est obligatoire</p>';
     	}
     	if ($_POST['descriptionIn']=="")
     	{
     		$error .='<p style="color:red">Le champ "Déscription" est obligatoire</p>';
     	}
     	
     	if ($error=="")
     	{
     		$server = 'localhost'; // MySql Server
     		$username = 'root'; // MySql Username
     		$password = NULL ; // MySql Passwort
     		$database = 'telethondb'; // MySql Datenbankname
     		$con = mysql_connect($server,$username,$password);
     		if (!$con)
     		{
     			die('La Connexion avec la Base de Données est impossible!: ' . mysql_error());
     			$error='<p style="color:red">La Connexion avec la Base de Données est impossible!</p>';
     		}
     		else
     		{
     			mysql_select_db($database) or die ("Base de Données 'Compte' n'existe pas!");
     
     			$result = mysql_query("INSERT INTO activity (appellation,genre,description)
				VALUES ('".$_POST['nameIn']."', '".$_POST['genreIn']."', '".$_POST['descriptionIn']."')");
     			
     			$equipesqlresult = mysql_query("INSERT INTO equipe (activity) VALUES ('".$_POST['nameIn']."')");
     			
     			$activityName= $_POST['nameIn'];

     			$sql = "UPDATE compte ".
     					"SET activite = '".$activityName ."' ".
     					"WHERE userName = '".$identifiant."'";
     			
     			$result1 = mysql_query($sql,$con);
				
     			if($result1)
     			{
     				$_SESSION["activite"]= $activityName;
     			}
     			
       					
     			$error='<p style="color:green">Votre nouvelle Activité est crée!</p>';
     		}
     
     
     	}
     }
?> 
<!DOCTYPE html>
<html>

	<head>
		<title>Esigelec-Telethon- Accueil</title>
		<meta content="text/html;charset=utf-8" http-equiv="content-type">
		<link rel = "stylesheet" type = "text/css" href="css/allstyles.css">
		
	</head>
	
	<body>

	<div id="workspace" class="workspace">

			<div id="HomeBody">

				<table 
					style="table-layout: fixed; margin: 0px auto;">
					<tbody>
						<tr>

							<td class="HomeLeftTDContainer">

								<div style="width: 320px;">

									<div>

										<div id="topcontainer">
											<a class="fimlogo" href="index.php"
												title="Home"></a>
										</div>
									</div>
									
									<img src="images/oser_vaincre.png" alt="Telethon"/>


								</div>
							</td>

							<td >
								<div>
									<div>
										<span class="boldtext" style="float:right;padding-right:15px;"> 
												
												<?php 
												if (isset($loggedin) || $loggedin) echo $nom; 
												?> 
										</span>
										<div>
											
											<div class="menu">
												<ul class="menulinkUL">
													<li class="li_menulink"><a class="menulink" href="moncompte.php"
														title="Mon compte">Mon compte</a></li>
													<li class="li_menulink"><a class="menulink" href="monequipe.php"
														title="Mon équipe">Mon équipe</a></li>
													<li class="li_menulink"><a class="menulink_first" href="myactivity.php"
														title="Créer Activité">Créer Activité</a></li>	
													<li class="li_menulink"><a class="menulink" href="reactivity.php"
														title="Rejoindre Activité">Rejoindre Activité</a></li>				
													<li class="li_menulink"><a class="menulink" href="deconnexion.php"
														title="Déconnexion">Déconnexion</a></li>													
												</ul>

											</div>
											
											<div class="ClearBoth">
													<span></span>
											</div>
											
											<div class="UIHomePage">

												<div class="TLContainer">

														<div class="overview">
															
															
															<div class="FORMULAR-PANEL">
															<div>
																<span class="ctitle" >
																	Créer une activité:
																</span>
																 
															</div>
															
															<br>	
																<form name="activityForm" id="activityForm" method="post" action="" target="">
																
																<label for="nom" style="width: 105px;float: left;">Nom:</label>
																<input type="text" name="nameIn" id="nom" value=""/>
																<br /><br />
																<label for="genre" style="width: 105px;float: left;">Genre:</label>
																<input type="text" name="genreIn" id="genre" value=""/>(Education, Sports, loisirs, ...)
																<br /><br />
																<label for="description" style="width: 105px;float: left;">Déscription:</label>
																<textarea name="descriptionIn" cols="26" rows="6" 
																style="width: 300px;font-size:12px;font-family: arial;"  
																id="description"></textarea>		  
																
																<br /><br />
				
																<p>
																<input type="submit" name="newactivity" value="Créer" style="float:right"/>
																</p>
																
																</form>
																<br/><br/>
																<?php if (isset($error)) echo $error; ?>
															</div>
														</div>
												</div>
												<div class="ClearBoth">
													<span></span>
												</div>
											</div>

										</div>

									</div>

								</div>

							</td>

						</tr>
					</tbody>
				</table>


			</div>

			<div>
				<div >
					<div>
						<div>
							<div>
								<div class="footer">
									<div id="areacontainer">
										<div id="area">

											<a title="Telethon 2013"
												href="moncompte.php">Esigelec - Telethon 2013</a>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
   </div>
  
  </body>
	
	
</html>