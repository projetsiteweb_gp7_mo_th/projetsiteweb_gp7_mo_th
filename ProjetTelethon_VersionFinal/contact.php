<?php

$sendto_email = "webmaster@esigelec-telethon.de";
$subject ="Informations";
$strDelimiter  = ":\t";


if (isset($_POST['info']))
	{
	$error="";
	if ($_POST['nameIn']=="")
	{
	$error='<p style="color:red">Le champ "Nom" est obligatoire!</p>';
	}
	if ($_POST['emailIn']=="")
	{
	$error .='<p style="color:red">Le champ "E-Mail" est obligatoire</p>';
	}
	
	if ($error=="")
	{
	$header = array();
    $header[] = "From: ".mb_encode_mimeheader($_POST['nameIn'], "utf-8", "Q")." <".$_POST['emailIn'].">";
    $header[] = "MIME-Version: 1.0";
    $header[] = "Content-type: text/plain; charset=utf-8";
    $header[] = "Content-transfer-encoding: 8bit";
    

 	$strMailtext = "";

 	while(list($strName,$value) = each($_POST))
 		{
 			if(is_array($value))
  			{
   				foreach($value as $value_array)
   				{
   				 $strMailtext .= $strName.$strDelimiter.$value_array."\n";
   				}
  			}
  			else
  			{
  			 $strMailtext .= $strName.$strDelimiter.$value."\n";
  			}
 	 	}

     mail($sendto_email,mb_encode_mimeheader($subject, "utf-8", "Q"),$strMailtext, implode("\n", $header));

     $error .='<p style="color:green">Votre Message a été envoyé!</p>';
	}
}
?>


<!DOCTYPE html>
<html>

	<head>
		<title>Esigelec-Telethon- Accueil</title>
		<meta content="text/html;charset=utf-8" http-equiv="content-type">
		<link rel = "stylesheet" type = "text/css" href="css/allstyles.css">
		
	</head>
	
	<body>

	<div id="workspace" class="workspace">

			<div id="HomeBody">

				<table 
					style="table-layout: fixed; margin: 0px auto;">
					<tbody>
						<tr>

							<td class="HomeLeftTDContainer">

								<div style="width: 320px;">

									<div>

										<div id="topcontainer">
											<a class="fimlogo" href="index.php"
												title="Home"></a>
										</div>
									</div>
									
									<img src="images/oser_vaincre.png" alt="Telethon"/>


								</div>
							</td>

							<td >
								<div>
									<div>

										<div>
											
											<div class="menu">
												<ul class="menulinkUL">
													<li class="li_menulink"><a class="menulink" href="index.php"
														title="Acceuil">Accueil</a></li>
													<li class="li_menulink"><a class="menulink" href="activites.php"
														title="Activités">Activités</a></li>
													<li class="li_menulink"><a class="menulink" href="inscription.php"
														title="Inscription">Inscription</a></li>
													<li class="li_menulink"><a class="menulink" href="inscrits.php"
														title="Inscrits">Inscrits</a></li>						
													<li class="li_menulink"><a class="menulink" href="connexion.php"
														title="Connexion">Connexion</a></li>
												</ul>

											</div>
											
											<div class="ClearBoth">
													<span></span>
											</div>
											
											<div class="UIHomePage">
												
												<div class="TLContainer">

														<div class="FORMULAR-PANEL">	
																<div>
																	<span class="ctitle" id="first_Header">
																		Contact:
																	</span>
																</div>
																<br/><br/>
																<form name="infoForm" id="infoForm" method="post" action="" target="">
																
																<label for="nom" style="width: 105px;float: left;">Nom:</label>
																<span>*</span>
																<input type="text" name="nameIn" id="nom" value=""/>
																<br /><br />
																<label for="email" style="width: 105px;float: left;">E-Mail:</label>
																<span>*</span>
																<input type="text" name="emailIn" id="email" value=""/>
																<br /><br />
																<label for="mail_message" style="width: 105px;float: left;">Votre Message:</label>
																<span style="color:white;">*</span>
																<textarea name="mail_message" cols="36" rows="10" style="width: 400px;font-size:12px;font-family: arial;"  id="mail_message"></textarea>		  
																<br/> 
				
																<p>
																<input type="submit" name="info" value="Envoyer" style="float:right"/>
																</p>
																
																</form>
																<br/><br/>
																<?php if (isset($error)) echo $error; ?>
														</div>
												</div>
												<div class="ClearBoth">
													<span></span>
												</div>
											</div>

										</div>

									</div>

								</div>

							</td>

						</tr>
					</tbody>
				</table>


			</div>

			<div>
				<div >
					<div>
						<div>
							<div>
								<div class="footer">
									<div id="areacontainer">
										<div id="area">

											<a title="Telethon 2013"
												href="index.php">Esigelec - Telethon 2013</a>
											
											- <a title="Contact"
												href="contact.html">Contact</a>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
   </div>
  
  </body>
	
	
</html>